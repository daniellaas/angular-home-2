import { Injectable } from '@angular/core';
import {observable, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];
  
  getBooks(){
    const bookObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.books),5000
        )
      }
    )
    return bookObservable;
  }

  addBooks(){
    setInterval(
      () => this.books.push({title:'A new book', author:'new author'})
    )
  }

  /*getbooks(){
    //return this.books;
    setInterval(()=>this.books,1000)
  }*/

  constructor() { }
}
