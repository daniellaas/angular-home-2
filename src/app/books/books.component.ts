import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  books: any;
  books$:Observable<any>;

  constructor(private bookservice:BooksService) { }

  ngOnInit() {
    this.books$ = this.bookservice.getBooks();
    this.bookservice.addBooks();
  }

}
